import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename
from ..tasks import labeling_async_task


class LabelingController(Controller):

    @route('/labeling/<string:project>', methods=['POST'])
    def labeling(self, project):
        labeling_async_task.delay(project)
        return jsonify(success=True)

    @route('/status/<string:project>', methods=['GET'])
    def status(self, project):
        return jsonify(success=True)

    @route('/logs/<string:project>', methods=['GET'])
    def logs(self, project):
        redis = AppConfig.SESSION_REDIS
        key = 'labeling_logger_' + project
        logged = redis.get(key) or "{\"logs\":[]}"
        logged = json.loads(logged)
        return jsonify(project=project, logs=logged['logs'])
