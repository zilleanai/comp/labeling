import os
import gc
import json
from io import StringIO, BytesIO
import pickle
import multiprocessing

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from zworkflow import Config
from zworkflow.dataset import get_dataset
from zworkflow.model import get_model
from zworkflow.label import get_label

def labeling_logger(project, *args, **kwargs):
    redis = AppConfig.SESSION_REDIS
    key = 'labeling_logger_'+project
    logged = redis.get(key) or "{\"logs\":[]}"
    logged = json.loads(logged)
    mapped = "".join(map(str, args))
    logged['logs'].append(mapped)
    redis.setex(key, 600, json.dumps(logged))

@celery.task(serializer='pickle')
def labeling_async_task(project):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
    configfile = os.path.join('workflow.yml') or {}
    config = Config(configfile)
    config['general']['verbose'] = True

    logger = lambda *args, **kwargs: labeling_logger(project, args, kwargs)
    label = get_label(config)
    if config['general']['verbose']:
        print(label)
    label.label(logger=logger)
