# labeling

Component for labeling.


## Installation

```yml
# mlplatform-domain.yml

name: domain
comps:
 - https://gitlab.chriamue.de/mlplatform/comp/labeling.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.labeling',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.labeling.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Labeling
} from 'comps/labeling/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Labeling: 'Labeling',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Labeling,
    path: '/labeling',
    component: Labeling,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Labeling} />
    ...
</div>
```
