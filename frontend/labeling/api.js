import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function labelingUri(uri) {
  return v1(`/labeling${uri}`)
}

export default class Labeling {

  static labeling() {
    return post(labelingUri(`/labeling/${storage.getProject()}`), {})
  }


  static loadLogs() {
    return get(labelingUri(`/logs/${storage.getProject()}`))
  }

}
