import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import './labeling-logs.scss'
import { v1 } from 'api'
import { storage } from 'comps/project'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { loadLogs } from 'comps/labeling/actions'
import { selectLogsById } from 'comps/labeling/reducers/logs'

class LabelingLogs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentWillMount() {
    const { loadLogs } = this.props
    const project = storage.getProject()
    loadLogs.maybeTrigger({ project })
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      const { loadLogs } = this.props
      const project = storage.getProject()
      loadLogs.maybeTrigger({ project })
      this.setState({ time: Date.now() })
    }, 3000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { isLoaded, logs, error } = this.props
    if (!isLoaded) {
      return null
    }

    return (
      <div className="LabelingLogs">
        {logs.map((log, i) => {
          return (
            <li key={i} >
              {log}
            </li>
          )
        })}
      </div>
    );
  }
}

const withConnect = connect(
  (state, props) => {
    const project = storage.getProject()
    const logs = selectLogsById(state, project)
    return {
      logs,
      isLoaded: !!logs,
    }
  },
  (dispatch) => bindRoutineCreators({ loadLogs }, dispatch),
)

const withReducer = injectReducer(require('comps/labeling/reducers/logs'))
const withSaga = injectSagas(require('comps/labeling/sagas/logs'))

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LabelingLogs)
