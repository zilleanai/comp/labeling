import { call, put, takeLatest } from 'redux-saga/effects'

import { labeling } from 'comps/labeling/actions'
import { createRoutineFormSaga } from 'sagas'
import LabelingApi from 'comps/labeling/api'


export const KEY = 'labeling'

export const labelingSaga = createRoutineFormSaga(
  labeling,
  function* successGenerator(payload) {
    const response = yield call(LabelingApi.labeling, payload)
    yield put(labeling.success(response))
  }
)

export default () => [
  takeLatest(labeling.TRIGGER, labelingSaga)
]
