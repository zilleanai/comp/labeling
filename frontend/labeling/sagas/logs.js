import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadLogs } from 'comps/labeling/actions'
import LabelingApi from 'comps/labeling/api'
import { selectLogs } from 'comps/labeling/reducers/logs'


export const KEY = 'logs'

export const maybeLoadLogsSaga = function* ({payload:project}) {
  const { byId, isLoading } = yield select(selectLogs)
  const isLoaded = !!byId[project.project]
  if (!(isLoaded || isLoading)) {
    yield put(loadLogs.trigger())
  }
}

export const loadLogsSaga = createRoutineSaga(
  loadLogs,
  function* successGenerator() {
    const logs = yield call(LabelingApi.loadLogs)
    yield put(loadLogs.success(logs))
  }
)

export default () => [
  takeEvery(loadLogs.MAYBE_TRIGGER, maybeLoadLogsSaga),
  takeLatest(loadLogs.TRIGGER, loadLogsSaga),
]
