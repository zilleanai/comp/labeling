import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { v1 } from 'api'
import Helmet from 'react-helmet'
import { Button, ButtonGroup } from 'react-bootstrap';
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { InfoBox, PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { LabelingStatus, LabelingLogs } from 'comps/labeling/components'
import { labeling } from 'comps/labeling/actions'

class Labeling extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.labeling-page',
          intro: 'This page allows to label data.',
        },
        {
          element: '.button-primary',
          intro: 'Pressing label button starts the label.py script in the project folder.',
        },
      ],
    }
  }

  render() {
    const { isLoaded, error, pristine, submitting } = this.props
    if (!isLoaded) {
      return null
    }
    const { stepsEnabled, steps, initialStep } = this.state
    return (
      <PageContent className='labeling-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Labeling</title>
        </Helmet>
        <h1>Labeling!</h1>
        <div className="row">
          <button type="submit"
            className="button-primary"
            onClick={(e) => {
              this.props.labeling.trigger({})
            }}
            disabled={pristine || submitting}
          >
            {submitting ? 'Labeling...' : 'Label'}
          </button>
        </div>
        <LabelingStatus />
        <LabelingLogs />
      </PageContent>)
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    return {
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ labeling }, dispatch),
)


const withSaga = injectSagas(require('comps/labeling/sagas/labeling'))

export default compose(
  withConnect,
  withSaga,
)(Labeling)

