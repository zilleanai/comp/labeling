import { createRoutine } from 'actions'

export const labeling = createRoutine('labeling/LABELING')
export const loadLogs = createRoutine('labeling/LOGS')